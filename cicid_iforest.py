import pandas as pd
import numpy as np
from sklearn.ensemble import IsolationForest
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')

#Settings
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)

cols = [' Bwd Packet Length Std',' PSH Flag Count',' min_seg_size_forward',' Min Packet Length',' ACK Flag Count',' Bwd Packet Length Min',' Fwd IAT Std','Init_Win_bytes_forward',' Flow IAT Max',' Bwd Packets/s',' URG Flag Count','Bwd IAT Total',' Label']
df1=pd.read_csv("Friday-WorkingHours-Afternoon-DDos.pcap_ISCX.csv", usecols = cols   )#,nrows = 50000
df2=pd.read_csv("Friday-WorkingHours-Afternoon-PortScan.pcap_ISCX.csv", usecols = cols )
df3=pd.read_csv("Friday-WorkingHours-Morning.pcap_ISCX.csv", usecols = cols )
df5=pd.read_csv("Thursday-WorkingHours-Afternoon-Infilteration.pcap_ISCX.csv", usecols = cols )
df6=pd.read_csv("Thursday-WorkingHours-Morning-WebAttacks.pcap_ISCX.csv", usecols = cols )

# data preprocessing

df = pd.concat([df1, df2])
del df1, df2
df = pd.concat([df, df3])
del df3
df = pd.concat([df, df5])
del df5
df = pd.concat([df, df6])
del df6

data = df.copy()


y = data[' Label'].copy()
X = data.drop([' Label'],axis=1)
X = pd.DataFrame(X)


# Machine learning model

scaler = StandardScaler()

rng = np.random.RandomState(42)

model = IsolationForest(max_samples=10000, random_state=rng)
model.fit(X)
y_pred = model.predict(X)
print(y_pred)
print(y_pred.shape)

print("percentage of Legit:", (list(y_pred).count(1)/y_pred.shape[0])*100)
print("percentage of Anomaly:", (list(y_pred).count(-1)/y_pred.shape[0])*100)

# Average anomaly score of X of the base classifiers.
# print(len(model.decision_function(X)))
#scores = model.score_samples(X)
#plt.hist(scores)
#plt.show()

y.unique()
y_true=y.copy()
attack = ['DDoS', 'PortScan', 'Bot', 'Infiltration', 'Web Attack � Brute Force', 'Web Attack � XSS', 'Web Attack � Sql Injection']
normal = 'BENIGN'
y_true=y_true.replace(attack, -1)
y_true=y_true.replace(normal, 1)
y_true.unique()

print (len(y_true))
y_true.value_counts()

print(len(y_pred))
pd.Series(y_pred).value_counts()

cf_matrix = confusion_matrix(y_true, y_pred)
tn, fp, fn, tp = cf_matrix.ravel()
print(cf_matrix)


ax = sns.heatmap(cf_matrix, annot=True, cmap='Blues')

ax.set_title('Confusion Matrix with labels\n\n');
ax.set_xlabel('\nPredicted Values')
ax.set_ylabel('Actual Values ');

## Ticket labels - List must be in alphabetical order
ax.xaxis.set_ticklabels(['False','True'])
ax.yaxis.set_ticklabels(['False','True'])

## Display the visualization of the Confusion Matrix.
plt.show()

print ("True Negetive", tn,
       "\nTrue Positive", tp)
print ("False Negetive", fn,
       "\nFalse Positive", fp)

recall = tp/(tp+fn)
precision = tp/(tp+fp)
print("Recall", recall, "\nPrecision", precision)

f1 = 2 * (precision*recall)/(precision+recall)
print("F1 Score", f1)


